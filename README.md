# Ball Detection - Computer Vision

## 1. Project Overview
*  Using Python and the OpenCV library, write a script to detect a ball of a certain color.
*  Plan:
    1.  Install Anaconda/Jupyter notebook
    2.  Given an image of colorful balls, program a script to detect a ball of a color of your choosing.
    3.  Use the code from the previous step to detect a colored pom pom from a live video feed.

## 2. Install Anaconda
Anaconda is the premier data science platform for Python. It comes with a slew of features and packages.
We'll be using the Jupyter notebook editor to write and test our CV code.

1. [Click on this link to download Anaconda](https://www.anaconda.com/distribution/)
2. **Important: The default download is for Mac...Windows users should click on the windows tab...DUH**
3. Click "Download" button for Python 3.7 version
4. Run the graphical installer, click next thru menus, accept all defaults...yadayada

## 3. Getting started
We'll be doing all our Python coding in Jupyter Notebook, a neat little code editor that comes with Anaconda.
1.  [Here is the repository with the starter code](https://gitlab.com/edjang0/duke-robotics-cv/tree/master/ball_detection) 
2.  Click the download icon in the top right and click zip.
2.  Unzip the folder you downloaded into a directory of your liking.
3.  Search for and open the Anaconda Prompt application
4.  Type `cd the/path/to/the/folder/you/unzipped` and hit enter...e.g. `cd C:\Users\edj9\Documents\robosub_cv`
5.  Type `jupyter notebook` and hit enter.
6.  A new chrome window should open. Click on script.py.

## 4. Let's get coding!
Pick an image from the sample images included in the folder. I chose blue.jpg. Your task is to draw an image around the colored ball!

Original Image

![](sample_images/1.PNG)

`cv2.GaussianBlur` to remove jagged edges and make contouring easier
![](sample_images/2.PNG)

`cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)` to convert image to HSV space
![](sample_images/3.PNG)

`cv2.inRange(hsv, blue_min, blue_max)` to color mask the image, making all blue pixels white and any other pixels black.
![](sample_images/4.PNG)

`cv2.findContours()` to find outlines of white pixels
![](sample_images/5.PNG)

Filter out any contours which aren't circular, and only draw circular contours to the final image
![](sample_images/6.PNG)